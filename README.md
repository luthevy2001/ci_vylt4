## Requirements

- Python --version 3.7 or above to run the project 
    - Download Python at [https://www.python.org/downloads/](https://www.python.org/downloads/)
- Python libraries:
    - from asyncio.windows_events import NULL
    - import sqlite3 (included in the standard library since Python 2.5).
    - import schedule (pip install schedule)
    - import requests (pip install requests)
    - from datetime import datetime,timedelta (pip install datetime)
    - import json (built-in module)
- Environment: Visual Studio Code, with listed extensions:
    - Python (to run Python source)
    - SQLite Viewer (to preview SQLite tables)
_____________
## SOURCE FOLDER
- `src/main.py`: run this file, which contains whole process
- `src/etl_loading.py`: contains database operations
- `src/utilities.py`: contains file-database processing functions
- `response_description.txt`: API data description
- `src/weather.json`: response value from API, saved in JSON format
- `db/weather.db`: SQLite database
- `tests/test_extract.py`: unit-test source using pytest for ETL Extract phase
- `tests/test_transform.py`: unit-test source using pytest for ETL Transform phase

_____________
## HOW TO RUN
1. Install Python 3.x version, VS Code (https://code.visualstudio.com/), listed above libraries and extensions
2. Run main.py file
    - "Enter file name (i.e: weather.json)": give JSON file a name, this file will store crawled data
    - "Enter database name (i.e: weather.db)": give SQLite database a name
    - "Enter location": city name (i.e: Hanoi, Moscow, New York, Tokyo, Havana, Seoul, Saigon, Paris, Berlin,..)
    - "Enter unit": metric or imperial
3. If there is no error, the program will produce 2 new files (auto-generate): <name>.json and <name>.db (with crawled data)
_______________
## Note
- I chose SQLite3. Because it is a light-weight, very easy-to-use SQL database engine. SQLite3 doesn't require complex implementation process, it is already included in the standard library since Python 2.5
- Tasks done: 
    - Using python to call API
    - Understand the data and create schema for it
    - Create schema in sql using python connector (using SQLite3)
    - Connect to sql and ETL data
    - Create schedule for calling api

