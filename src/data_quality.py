import numpy as np
import pandas as pd

def missing_ratio(col):
    return col.isna().mean() * 100

def check_minmax_missing_num(df):
    df = df.replace(0, np.nan)
    num_col_profiles_df1 = df[["main_mp", "main_fl_lik", "main_prur","main_humidiy","main_mp_min","main_mp_max","main_a_lvl"]]
    num_col_profiles_df1 = num_col_profiles_df1.agg([missing_ratio, pd.Series.min, pd.Series.max])
    print(num_col_profiles_df1)
    num_col_profiles_df2 = df[["main_grnd_lvl", "viibiliy","wind_pd","wind_dg","wind_gu","cloud","rain_1h","rain_3h","snow_1h","snow_3h"]]
    num_col_profiles_df2 = num_col_profiles_df2.agg([missing_ratio, pd.Series.min, pd.Series.max])
    print(num_col_profiles_df2)

def unique_non_null(s):
    return s.dropna().unique()

def check_minmax_missing_cate(df):
    cate_col_profiles_df = df[["wahr_id", "ba"]]
    cate_col_profiles_df = cate_col_profiles_df.agg([unique_non_null, pd.Series.nunique])
    cate_col_profiles_df = cate_col_profiles_df.rename(index={'nunique': 'num_diff_vals'})
    print(cate_col_profiles_df)

def check_datatype(df):
    return df.dtypes
    
def check_null(df):
    return df.isnull().sum()
