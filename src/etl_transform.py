from datetime import datetime,timedelta
import json


def from_unix_to_dt(df):
	return datetime.utcfromtimestamp(df).strftime('%d/%m/%Y, %H:%M:%S')

def from_unix_to_timezone(df):
	return str(timedelta(seconds=df))

def json_preprocessing(file_name):
	""" Transform value of json object into suitable type

	Transform phase in ETL data process  
	
	:param: str file_name : name of json file
	:return: transformed data
	"""
	with open(file_name,'r+') as json_file:
		data = json.load(json_file) 
		data["d"]=from_unix_to_dt(data["d"])
		data['imzon']=from_unix_to_timezone(data['imzon'])
		data['y']['unri']=from_unix_to_dt(data['y']['unri'])
		data['y']['un']=from_unix_to_dt(data['y']['un'])
		data['y']['id']=str(data['y']['id'])
		data['y']['yp']=str(data['y']['yp'])
		data['id']=str(data['id'])
		data['cod']=str(data['cod'])
		data['wahr']['id']=str(data['wahr']['id'])
		json_file.seek(0)
		json.dump(data, json_file)
		json_file.truncate()


from_unix_to_timezone(25200)
from_unix_to_timezone(3900)
from_unix_to_timezone(36000)
from_unix_to_timezone(14400)
from_unix_to_dt(1654099054)
from_unix_to_dt(154099054)
from_unix_to_dt(184099054)
from_unix_to_dt(174099054)
