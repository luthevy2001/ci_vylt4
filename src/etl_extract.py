from src.etl_loading import load_data
from src.sample_data import sd

def write_json(response,file_name):
    """ Save data from API into JSON file

    :param: str response : data from API
            str file_name: name of JSON file
    :return: new JSON file filled with weather data
    """
    disallowed_characters = "()test[]"
    tmp = response
    # Remove specific characters in response string (which has form "test(<data_in_json_format>)")
    for character in disallowed_characters:
        tmp = tmp.replace(character, "")
    try:
        with open(file_name, 'w', encoding='utf-8') as f:
            f.write(tmp)
            print("Saving data successfully!")
    except:
        print("An exception occurred!")
	
def get_city_info(cityname):
    if load_data(" ","nam","src/weather.json") == cityname:
        lon=load_data("coord","lon","src/weather.json")
        la= load_data("coord","la","src/weather.json")
        imzon= load_data("","imzon","src/weather.json")
        id=load_data("","id","src/weather.json")
        cod=load_data("","cod","src/weather.json") 
        res={ 
                "coord": {
                    "lon": lon,
                    "la": la
                },
                "imzon": imzon,
                "id": id,
                "nam": cityname,
                "cod": cod
            } 
        return res

get_city_info('Hanoi')
get_city_info('Havana')
get_city_info('Ho Chi Minh City')
get_city_info('Seoul')
get_city_info('Bangkok')
get_city_info('Paris')
get_city_info('Berlin')
get_city_info('Beijing')

