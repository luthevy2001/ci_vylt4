from asyncio.windows_events import NULL
import requests
from etl_extract import write_json
import sqlite3
import pandas as pd

def crawl_data(loc,unit,file_name):
    """ Crawl data from API process
    Based on given location and unit type, crawl weather data of that location

    :param: str loc : name of city
            str unit: unit type, metric or imperial
            str file_name : name of JSON file
    :return: new JSON file filled with weather data
    """
    url = "https://community-open-weather-map.p.rapidapi.com/weather"
    querystring = {"q":loc,"lat":"0","lon":"0","callback":"test","id":"2172797","lang":"en","units":unit,"mode":"xml"}
    headers = {
		"X-RapidAPI-Host": "community-open-weather-map.p.rapidapi.com",
		"X-RapidAPI-Key": "fe2fdce76emsh9ba685183b38824p19c726jsn087604409efc"
	}
    try:
        response = requests.request("GET", url, headers=headers, params=querystring)
        write_json(response.text,file_name)
    except:
        print('City not found! Type again or try another')

def get_repo():
    # Display current working repository (to display hidden database location)
	import os
	print(os.getcwd())

def db_to_dataframe():
    # Create your connection.
    conn = sqlite3.connect('db\weather.db')
    df = pd.read_sql_query("SELECT * FROM WEATHER_FACT", conn)
    conn.close()
    return df

