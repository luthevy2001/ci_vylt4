import pytest
from src.etl_extract import get_city_info
from src.sample_data import sd

@pytest.mark.parametrize("cityname, info",[('Hanoi', sd["city_info"] )])
def test_get_city_info(cityname,info):
    assert get_city_info(cityname) == info

