import pytest
from src.etl_transform import from_unix_to_dt
from src.etl_transform import from_unix_to_timezone


@pytest.mark.parametrize("unix, dt",[(1654099054, "01/06/2022, 15:57:34"),
                                     (1644155305, "06/02/2022, 13:48:25"),
                                     (1604155305, "31/10/2020, 14:41:45"),
                                     (1654249486, "03/06/2022, 09:44:46"),
                                     (1054249486, "29/05/2003, 23:04:46"),
                                     (1454249486, "31/01/2016, 14:11:26"),
                                     (1556249486, "26/04/2019, 03:31:26"),
                                     (1656249486, "26/06/2022, 13:18:06")])
def test_unix_to_dt(unix,dt):
    assert from_unix_to_dt(unix) == dt

@pytest.mark.parametrize("sec, timezone",[(25200, "7:00:00"),
                                          (10800, "3:00:00"),
                                          (3600,  "1:00:00"),
                                          (7200,  "2:00:00"),
                                          (21600, "6:00:00"),
                                          (18000, "5:00:00"),
                                          (14400, "4:00:00"),
                                          (28800, "8:00:00"),
                                          (39600, "11:00:00")])
def test_unix_to_timezone(sec,timezone):
    assert from_unix_to_timezone(sec) == timezone


